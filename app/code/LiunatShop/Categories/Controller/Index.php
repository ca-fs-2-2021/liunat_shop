<?php

namespace LiunatShop\Categories\Controller;

use LiunatShop\Framework\Helpers\FormBuilder;
use LiunatShop\Framework\Helpers\Request;
use LiunatShop\Categories\Model\Category;
use LiunatShop\Products\Model\Product;
use LiunatShop\Framework\Helpers\Url;
use LiunatShop\Products\Model\Collection\Products;
use LiunatShop\Framework\Core\Controller;

class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request = new Request();
        parent::__construct('LiunatShop\Categories');
    }

    public function index()
    {
        $category = new Category();
        $category->load(2);
    }

    public function create()
    {
        $allParentIds = Category::getAllCollumnsFromTable("id");
        $allParentIds[] = "Root Category";

        $formHelper = new FormBuilder('POST', Url::getUrl("categories/store/"), 'form', 'category-form');
        $formHelper->input('text', 'name', 'text-field', 'category-name', 'Category Name');
        $formHelper->input('text', 'description', 'text-field', 'category-name', 'Category Description');
        $formHelper->select('parent_id', $allParentIds ,'Parent ID');
        $formHelper->button('ok', 'create');

        $data['form'] = $formHelper->get();
        $data['title'] = "Here you can create Category.";
        $this->render('form\create', $data);
    }

    public function store()
    {
        $name = $this->request->getPost('name');
        $description = $this->request->getPost('description');
        $parentId = $this->request->getPost('parent_id');
        
        $category = new Category();
        $category->setName($name);
        $category->setDescription($description);
        $category->setParentId($parentId);

        if ($category->checkNameUnique($name) === true) {
            $category->save();
        }
        var_dump($_POST);
    }

    public function show()
    {
        $id = $this->request->getPost('id');

        $formHelper = new FormBuilder('POST', URL::getUrl("categories/show"), 'form', 'category-form');
        $formHelper->input('text', 'id', 'text-field', 'category-id', 'Category Id');
        $formHelper->button('show', 'Show One Category');
        $formHelper->button('showAll', 'Show All Categories');
        $category = new Category();

        $data['categories'] = $category->load($id);
        $data['title'] = "Choose category ID to see category information.";
        $data['form'] = $formHelper->get();
        $this->render('form\show', $data);


        // var_dump($_POST);
        // if ($id !== null || array_key_last($_POST) === "showAll") {
        //     $category->load($id);
        // }
    }

    // public function show($id)
    // {
    //     $productsModel = new Products();
    //     $productsModel->addCategoryFilter($id);
    //     $products = $productsModel->getCollection();

    // }





    public function edit()
    {
        $idHrefFromShow = array_key_first($_POST);

        $edit = new FormBuilder('POST', "http://www.shop.test/index.php/categories/update", 'form', 'category-form');
        $edit->input('text', 'id', 'text-field', '', '', 'ID', "$idHrefFromShow")
        ->button('edit', 'ID Of category You Want To Edit');

        $data['form'] = $edit->get();
        $data['title'] = "You Can Choose Category To Edit.";
        
        $this->render('form\edit', $data);
    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $category = new Category();
        $category->load($id);

        $id = $category->getId();
        $name = $category->getName();
        $description = $category->getDescription();
        $parentId = $category->getParentId();

        $uniqueCategorieIds = Product::getAllCollumnsFromTable("id");
        $uniqueCategorieNames = Product::getAllCollumnsFromTable("name");

        $productNamesAndIds = array_combine($uniqueCategorieNames, $uniqueCategorieIds);

        $update = new FormBuilder('POST', "http://www.shop.test/index.php/categories/update/$id", 'form', 'category-form');
        $update->input('hidden', 'id', '', '', '', '', "$id")
        ->input('text', 'name', 'text-field', '', '', 'name', "$name")
        ->input('text', 'description', 'text-field', '', '', 'description', "$description")
        ->input('text', 'parentId', 'text-field', '', '', 'parentId', "$parentId");
            foreach ($productNamesAndIds as $name => $id): 
                $update->input('checkbox', "products[]", 'class', "$id", '', "$name", "$id");
            endforeach;
        $update->button('update1', 'UPDATE');
        
        echo "Here You Can Edit Properties Of Chosen Category.";
        echo $update->get();

        if (array_key_last($_POST) === "update1") {
            $category->save();
        }
    }

    public function delete()
    {
        $id = $this->request->getPost('id');
        $category = new Category();
        
        $idHrefFromShow = array_key_first($_POST);

        $delete = new FormBuilder('POST', "delete/" . $id, 'form', 'category-form');
        $delete->input('text', 'id', 'text-field', '', 'ID', "", " $idHrefFromShow")->button('delete', 'Delete');

        echo "HERE YOU CAN DELETE A CATEGORY.";
        echo $delete->get();

        $category->delete($id);
    }
}



// public function show()
// {
//     $id = $this->request->getPost('id');

//     $formHelper = new FormBuilder('POST', "show/$id", 'form', 'category-form');
//     $formHelper->input('text', 'id', 'text-field', 'category-id', 'Category Id');
//     $formHelper->button('show', 'Show One Category');
//     $formHelper->button('showAll', 'Show All Categories');
//     echo "Choose category ID to see category information.";
//     echo $formHelper->get();



//     $category = new Category();

//     var_dump($_POST);
//     if ($id !== null || array_key_last($_POST) === "showAll") {
//         $category->load($id);
//     }
// }