<?php

namespace LiunatShop\Categories\Model\Collection;

use LiunatShop\Framework\Helpers\SqlBuilder;
use LiunatShop\Categories\Model\Category;

class Categories
{
    private  $collection = [];

    public function __construct()
    {   
        $this->initCollection();
        return $this;
    }   

    //copy and adapt this method to sql builder
    private function cleanResults($result)
    {
        $cleanArray = [];
        foreach ($result as $element) {
            foreach ($element as $line) {
                $cleanArray[] = $line;
            }
        }
        return $cleanArray;
    }

    public function addProductFilter($id)
    {
        $db = new SqlBuilder();
        $categoriesIds = $db->select('category_id')->from('connections')->where('product_id', $id)->getAll();
        $categoriesIds = $this->cleanResults($categoriesIds);
        foreach ($this->collection as $category) {
            if (!in_array($category->getId(), $categoriesIds)) {
                unset($this->collection[$category->getId()]);
            }
        }
    }

    public function addFilter($filter, $value, $operator)
    {

    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function initCollection()
    {
        $db = new SqlBuilder();
        $categoriesIds = $db->select('id')->from('categories')->getAll();
        foreach ($categoriesIds as $element) {
            $category = new Category();
            $this->collection[$element['id']] = $category->load($element['id']);
        }
    }


    public function getAllCategories(): array
    {
        $db = new SqlBuilder();
        $cateegories = $db->select()->from('categories')->getAll();
        return $cateegories;
    }
}