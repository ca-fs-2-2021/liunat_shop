<?php

use LiunatShop\Framework\Helpers\Request;
use LiunatShop\Categories\Model\Collection\Categories;

?>

<div class="wrapper">
    <h1><?= $data['title'] ?></h1>
    <?php echo $data['form']; ?>
</div>

<div>
    <?php
        $request = new Request();
        if (isset($data['categories']) && $request->getPost('id') !== "") {
            $id = $data['categories']->getId();
            $category = [
                'id' => $id,
                'name' => $data['categories']->getName(),
                'description' => $data['categories']->getDescription(),
                'parentId' => $data['categories']->getParentId(),
            ];
        }

        if ($data['categories']->getId() !== null) {
            $table = "<table class='table-stripped'>";
            $table .= "<tr>";
            foreach ($category as $header => $value) :
                $table .= "<th>$header</th>";
            endforeach;
            $table .= "<th>Edit</th><th>Delete</th></tr><tr>";
            foreach ($category as $categoryProp) :
                $table .= "<td>$categoryProp</td>";
            endforeach;
            $table .= "<td><form action='http://www.shop.test/index.php/categories/edit/$id' method='POST'>
                    <button type='submit' name='$id'>Edit</button></form></td>";
            $table .= "<td><form action='http://www.shop.test/index.php/categories/delete/$id' method='POST'>
                    <button type='submit' name='$id'>Delete</button></form></td></tr>";
            $table .= "</tr></table>";
            echo $table;
        }
    ?>
</div>

<div>
    <?php
    $categories = new Categories();
    $allCategoriees = $categories->getAllCategories();

    $table = "<table class='table-stripped'>";
    $table .= "<tr>";
    foreach ($allCategoriees[0] as $header => $value) {
        $table .= "<th>$header</th>";
    }
    $table .= "<th>Edit</th><th>Delete</th></tr>";

    foreach ($allCategoriees as $category) {
        $table .= "<tr>";
        foreach ($category as $categoryPropertyValue) {
            $id = $category['id'];
            $table .= "<td>$categoryPropertyValue</td>";
        }
        $table .= "<td><form action='http://www.shop.test/index.php/categories/edit/$id' method='POST'>
            <button type='submit' name='$id'>Edit</button></form></td>";
        $table .= "<td><form action='http://www.shop.test/index.php/categories/delete/$id' method='POST'>
            <button type='submit' name='$id'>Delete</button></form></td></tr>";
    }
    $table .= "</tr></table>";
    echo $table;
    ?>
</div>