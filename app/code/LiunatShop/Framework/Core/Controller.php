<?php

namespace LiunatShop\Framework\Core;

class Controller
{
    protected $moduleName;


    public function __construct($moduleName)
    {
        $this->moduleName = $moduleName;
    }

    public function render($template, $data)
    {       
        include_once PROJECT_ROOT_DIR . 'app\code\LiunatShop\Framework\view\header.php';
        include_once PROJECT_ROOT_DIR . 'app\code\\' . $this->moduleName . '\view\\' . $template . '.php';
        include_once PROJECT_ROOT_DIR . 'app\code\LiunatShop\Framework\view\footer.php';
    }
}