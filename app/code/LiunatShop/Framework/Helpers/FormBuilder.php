<?php

namespace LiunatShop\Framework\Helpers;

class FormBuilder
{
    private $form = '';

    public function __construct($method, $action, $class, $id)
    {
        $this->form .= "<form method='$method' action='$action' class='$class' id='$id'>";
        return $this;
    }

    public function input($type, $name, $class='', $id='', $placeholder='', $label='', $value='', $check='')
    {   
        $this->form .= "<div class='w-50 py-2'>";
        if($label !== '' && $id !== '') {
            $this->form .= "<label class='py-2' for='$id'>$label</label>";
        }
        $this->form .= "<input type='$type' name='$name' class='$class' id='$id' placeholder='$placeholder' value='$value' $check></div>";
        return $this;
    }

    public function button($name, $text, $class='')
    {
        $this->form .= "<button name='$name' class='$class'>$text</button>";
        return $this;
    }

    public function select($name, $options, $label) {
        $this->form .= "<div class='w-50'>";
        if($label !== '') $this->form .= "<label for='$name'>$label</label>";
        $this->form .= "<select id='$name' name='$name'>";
        foreach ($options as $option) {
            if ($option === "Root Category") {
                $this->form .= "<option value='0'>$option</option>";
            } else {
                $this->form .= "<option value='$option'>$option</option>";
            }
        }
        $this->form .= "</select></div><br>";
        return $this;
    }

    public function multiSelect($name, $options, $label) {
        $this->form .= "<div class='w-50'>";
        if($label !== '') $this->form .= "<label for='$name'>$label</label>";
        $this->form .= "<select id='$name' name='$name' multiple>";
        foreach ($options as $option) {
            if ($option === "Root Category") {
                $this->form .= "<option name='$option' value='0'>$option</option>";
            } else {
                $this->form .= "<option name='$option' value='$option'>$option</option>";
            }
        }
        $this->form .= "</select></div><br>";
        return $this;
    }

    public function textarea($name, $text, $class='')
    {
        $this->form .= "<div class='w-50'>";
        $this->form .= "
        <textarea name='$name' class='$class'>$text</textarea></div>";
        return $this;
    }

    public function link($href, $text, $class)
    {
        $this->form .= "<a class='$class' href='$href'>$text</a>";
        return $this;
    }

    public function get()
    {
        return $this->form .= "</form>";
    }
}