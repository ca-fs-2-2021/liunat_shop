<?php

namespace LiunatShop\Framework\Helpers;

class Request
{
    private $get;
    private $post;
    private $server;

    public function __construct()
    {
        $this->get = $_GET;
        $this->post = $_POST;
        $this->server = $_SERVER;
    }

    public function getRoute()
    {
        $url = [];
        if (!isset($this->server['PATH_INFO'])) {
            $url['controller'] = '/';
            return $url;
        }
        $uri = $this->server['PATH_INFO'];

        $uri = ltrim($uri, '/');
        $uri = rtrim($uri, '/');
        $uri = explode('/', $uri);

        if (isset($uri[0]) && $uri[0] !== '') {
            $url['controller'] = $uri[0];
            if (isset($uri[1])) {
                $url['method'] = $uri[1];
                if (isset($uri[2])) {
                    $url['param'] = $uri[2];
                }
            }
        } else {
            $url['controller'] = '/';
        }

        return $url;
    }

    public function getPost($key = null)
    {
        if ($key !== null) {
            return isset($this->post[$key]) ? $this->post[$key] : null;
        }
        return $this->post;
    }

    public static function getArrayKey($possition)
    {
        if ($possition === "first" && isset($_POST)) {
            $possition = 0;
            return array_keys($_POST)[0];
        } else if ($possition === "last" && count($_POST) > 1) {
            return array_keys($_POST)[count($_POST)-1];
        }
    }
}