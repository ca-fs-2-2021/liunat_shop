<?php

namespace LiunatShop\Framework\Helpers;

use LiunatShop\Errors\Controller\Index as Error;


class Routing
{
    public const ROUTING_MAP_FILE = 'routes.php';
    public const DEFAULT_CONTROLLER_METHOD = 'index';
    public const DEFAULT_ERROR_CONTROLLER = 'Error';
    public const DEFAULT_HOME_CONTROLLER = '\LiunatShop\Home\Controller\Index';

    public function getControllerClass($url)
    {
        include_once CONFIG_DIR . '/' . self::ROUTING_MAP_FILE;
        $method = self::DEFAULT_CONTROLLER_METHOD;
        $params = null;
        if ($url['controller'] !== '/') {
            if (isset($routes[$url['controller']])) {
                $controllerClass = $routes[$url['controller']];
                isset($url['method']) ? $method = $url['method'] : $method = self::DEFAULT_CONTROLLER_METHOD;
                isset($url['param']) ? $params = $url['param'] : $params = null;
            } else {
                $controllerClass = self::DEFAULT_ERROR_CONTROLLER;
                $method = 'error404';
            }
        } else {
            $controllerClass = self::DEFAULT_HOME_CONTROLLER;
        }
     
        $controller = new $controllerClass;
        if (method_exists($controller, $method)) {
            $params !== null ? $controller->$method($params) : $controller->$method();

        } else {
            $controller = new Error();
            $controller->error404();
        }
    }
}
