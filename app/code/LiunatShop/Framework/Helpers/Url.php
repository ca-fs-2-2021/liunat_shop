<?php

namespace LiunatShop\Framework\Helpers;

class Url
{
    public static function getUrl($path)
    {
        return APP_DOMAIN.$path;
    }

    public static function getCssUrl($file)
    {
        return APP_DOMAIN . "css/" . $file . ".css";
    }

    public static function getJsUrl($file)
    {
        return APP_DOMAIN . "js/" . $file . ".js";
    }

    public function getImageUrl($file)
    {
        return APP_DOMAIN . "image/" . $file;

    }
}