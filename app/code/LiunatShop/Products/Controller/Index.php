<?php

namespace LiunatShop\Products\Controller;

use LiunatShop\Categories\Model\Category;
use LiunatShop\Framework\Helpers\FormBuilder;
use LiunatShop\Framework\Helpers\Request;
use LiunatShop\Products\Model\Product;
use LiunatShop\Framework\Helpers\Url;
use LiunatShop\Framework\Core\Controller;
use LiunatShop\Products\Model\Collection\Products;

class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request = new Request();
        parent::__construct('LiunatShop\Products');
    }

    public function index()
    {
        $product = new Product();
        $product->load(2);
    }

    public function create()
    {
        $formHelper = new FormBuilder('POST', Url::getUrl("products/store/"), 'form', 'product-form');
        $formHelper->input('text', 'name', 'text-field', 'product-name', 'Product Name');
        $formHelper->input('text', 'sku', 'text-field', 'product-name', 'Product SKU');
        $formHelper->input('text', 'description', 'text-field', 'product-name', 'Product Description');
        $formHelper->input('text', 'price', 'text-field', 'product-name', 'Product Price');
        $formHelper->input('text', 'special_price', 'text-field', 'product-name', 'Product Special price');
        $formHelper->input('text', 'cost', 'text-field', 'product-name', 'Product Cost');
        $formHelper->input('text', 'qty', 'text-field', 'product-name', 'Product Qty');
        $formHelper->button('ok', 'create');
        
        $data['form'] = $formHelper->get();
        $data['title'] = "Here you can create new product.";
        $this->render('form\create', $data);
    }

    public function store()
    {
        $name = $this->request->getPost('name');
        $sku = $this->request->getPost('sku'); 
        $description = $this->request->getPost('description');
        $price = $this->request->getPost('price');
        $specialPrice = $this->request->getPost('special_price');
        $cost = $this->request->getPost('cost');
        $qty = $this->request->getPost('qty');

        $product = new Product();
        $product->setName($name);
        $product->setSku($sku);
        $product->setDescription($description);
        $product->setPrice($price);
        $product->setSpecialPrice($specialPrice);
        $product->setCost($cost);
        $product->setQty($qty);

        if ($product->checkSkuUnique($sku) === true) {
            $product->save();
        }
    }

    public function show()
    {
        $id = $this->request->getPost('id');

        $formHelper = new FormBuilder('POST', Url::getUrl("products/show/"), 'form', 'product-form');
        $formHelper->input('text', 'id', 'text-field', 'product-id', 'Product Id');
        $formHelper->button('showOne', 'Show One Product');

        $product = new Product();
        $data['products'] = $product->load($id);
        $data['title'] = "Choose product ID to see product information. <br>";
        $data['form'] = $formHelper->get();
        $this->render('form\show', $data);
    }

    public function edit($id)
    {
        $product = new Product();
        $product->load($id);

        $id = $product->getId();
        $name = $product->getName();
        $description = $product->getDescription();
        $sku = $product->getSku();
        $price = $product->getPrice();
        $specialPrice = $product->getSpecialPrice();
        $cost = $product->getCost();
        $qty = $product->getQty();

        $uniqueCategorieIds = Category::getAllCollumnsFromTable("id");
        $uniqueCategorieNames = Category::getAllCollumnsFromTable("name");
        $categoryNamesAndIds = array_combine($uniqueCategorieNames, $uniqueCategorieIds);

        $update = new FormBuilder('POST', Url::getUrl("products/update/$id"), 'form', 'product-form');
        $update->input('hidden', 'id', '', '', '', 'name', "$id")
            ->input('text', 'name', 'text-field', '', '', '', "$name")
            ->input('text', 'description', 'text-field', '', '', 'description', "$description")
            ->input('text', 'sku', 'text-field', '', '', 'sku', "$sku")
            ->input('text', 'price', 'text-field', '', '', 'price', "$price")
            ->input('text', 'special_price', 'text-field', '', '', 'special_price', "$specialPrice")
            ->input('text', 'cost', 'text-field', '', '', 'cost', "$cost")
            ->input('text', 'qty', 'text-field', '', '', 'qty', "$qty");
        foreach ($categoryNamesAndIds as $name => $id) :
            $update->input('checkbox', "categories[]", 'class', "$id", '', "$name", "$id");
        endforeach;
        $update->button('update', 'UPDATE')->link(URL::getUrl("products/show/"), "BACK", "btn");

        echo "Here You Can Edit Properties Of Chosen Product";
        echo $update->get();
    }

    public function update()
    {
        $request = new Request();
        $id = $request->getPost('id');
        $name = $request->getPost('name');
        $description = $request->getPost('description');
        $sku = $request->getPost('sku');
        $price = $request->getPost('price');
        $specialPrice = $request->getPost('special_price');
        $cost = $request->getPost('cost');
        $qty = $request->getPost('qty');
        $categories = $request->getPost('categories');

        $product = new Product();
        $product->load($id);
        $product->setName($name);
        $product->setDescription($description);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setSpecialPrice($specialPrice);
        $product->setCost($cost);
        $product->setQty($qty);
        $product->setCategories($categories);

        $product->save();
    }

    public function delete()
    {   
        if (count($_POST) === 1) {
            $id = Request::getArrayKey('first');
        } else {
            $id = $this->request->getPost('id');
        }
        $product = new Product();

        $delete = new FormBuilder('POST', Url::getUrl("products/delete/$id"), 'form', 'product-form');
        $delete->input('hidden', $id, 'text-field', '', 'ID', "", "$id")->button('delete', 'YES')
        ->link(Url::getUrl("products/show"), 'NO', 'btn');

        echo "ARE YOU SURE YOU WANT TO DELETE PRODUCT WITH ID: " . $id . " ?<br>";
        echo $delete->get();

        $product->delete();
    }

    public function import()
    {
        $url = 'https://manopora.lt/ca/spc.xml';
        $url = 'spc.xml';


        $path = PROJECT_ROOT_DIR . 'var\import\\';
        $fileName = 'products' . date('d-m-Y').'.xml';
        $file = $path.$fileName;
        $fp = fopen($file, 'w+');

        $curlHandler = curl_init($url);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $xml = $file;
        $xml = simplexml_load_file($file);


        foreach ($xml->produkt as $product) {
            $sku = (string)$product->kzs;
            if (Product::checkSkuUnique($sku)) {
                $name = (string)$product->nazwa;
                $qty = (int)$product->status;
                $price = (float)$product->cena_zewnetrzna;
                $specialPrice = (float)$product->vat;
                $cost = (float)$product->cena_zewnetrzna_hurt;
                $description = (string)$product->dlugi_opis;

                $newProduct = new Product();
                $newProduct->setSku($sku);
                $newProduct->setQty($qty);
                $newProduct->setName($name);
                $newProduct->setDescription($description);
                $newProduct->setSpecialPrice($specialPrice);
                $newProduct->setPrice($price);
                $newProduct->setCost($cost);
    
                $newProduct->save();
            }

        }
    }

    public function updateQty()
    {
        // Download data from source.
        $url = 'https://manopora.lt/ca/stocks.csv';
        $path = PROJECT_ROOT_DIR . 'var\import\\';
        $fileName = 'products' . date('d-m-Y').'.csv';
        $file = $path.$fileName;
        $fp = fopen($file, 'w+');
        $curlHandler = curl_init($url);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        // Data accumulation to array.
        $stockUpdate = [];

        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $stockUpdate[] = ['sku' => $data[0], 'qty' => $data[1], 'is_in_stock' => $data[2]];
            }
            fclose($handle);
            array_shift($stockUpdate);
        }
        $productsAdditionSku = [];
        $productsAdditionQty = [];
        
        foreach ($stockUpdate as $product) {
            $sku = (string)$product['sku'];
            $qty = (string)$product['qty'];
            if (!Product::checkSkuUnique($sku)) {
                $productsAdditionSku[] = $sku;
                $productsAdditionQty[] = $qty;
            }
        }

        $productsAddition = array_combine($productsAdditionSku, $productsAdditionQty);

        foreach ($productsAddition as $sku => $qty) {
            $product = new Product();
            $product->loadBySku($sku);
            $qtyInStock = $product->getQty();
            $product->setQty($qtyInStock + $qty);
         //   $product->save();
        }
    }
}