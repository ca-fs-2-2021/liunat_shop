<?php

namespace LiunatShop\Products\Model\Collection;

use LiunatShop\Framework\Helpers\SqlBuilder;
use LiunatShop\Products\Model\Product;

class Products
{
    private  $collection = [];

    public function __construct()
    {   
        $this->initCollection();
        return $this;
    }   

    //copy and adapt this method to sql builder
    private function cleanResults($result)
    {
        $cleanArray = [];
        foreach ($result as $element) {
            foreach ($element as $line) {
                $cleanArray[] = $line;
            }
        }
        return $cleanArray;
    }

    public function addCategoryFilter($id)
    {
        $db = new SqlBuilder();
        $productsIds = $db->select('product_id')->from('connections')->where('category_id', $id)->getAll();
        $productsIds = $this->cleanResults($productsIds);
        foreach ($this->collection as $product) {
            if (!in_array($product->getId(), $productsIds)) {
                unset($this->collection[$product->getId()]);
            }
        }
    }

    public function addFilter($filter, $value, $operator)
    {

    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function initCollection()
    {
        $db = new SqlBuilder();
        $productsIds = $db->select('id')->from('products')->getAll();
        foreach ($productsIds as $element) {
            $product = new Product();
            $this->collection[$element['id']] = $product->load($element['id']);
        }
    }


    public function getAllProducts(): array
    {
        $db = new SqlBuilder();
        $products = $db->select()->from('products')->getAll();
        return $products;
    }
}