<?php

namespace LiunatShop\Products\Model;

use LiunatShop\Framework\Helpers\SqlBuilder;
use LiunatShop\Framework\Helpers\Request;

class Product
{
    private $id;
    private $name;
    private $sku;
    private $description;
    private $price;
    private $specialPrice;
    private $cost;
    private $qty;
    private $categories;

    public function __construct($id='')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    public function getProduct($id='')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getprice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getQty()
    {
        return $this->qty;
    }

    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 


    public function load(?int $id)
    {
        if (array_key_last($_POST) === "showAll" && $id === "") {
            $this->loadAll();
            return $this;
        } else {
            $this->loadOne($id);
            return $this;
        }
    }

    public function loadOne(?string $id)
    {
        if ($id !== null && $id !== "") {
            $db = new SqlBuilder();
            $product = $db->select()->from('products')->where('id', $id)->getOne();
            $this->id = $product['id'];
            $this->name = $product['name'];
            $this->sku = $product['sku'];
            $this->description = $product['description'];
            $this->price = $product['price'];
            $this->specialPrice = $product['special_price'];
            $this->cost = $product['cost'];
            $this->qty = $product['qty'];
            return $this;
        }
    }

    public function loadBySku(?string $sku)
    {
        if ($sku !== null && $sku !== "") {
            $db = new SqlBuilder();
            $product = $db->select()->from('products')->where('sku', $sku)->getOne();
            $this->id = $product['id'];
            $this->name = $product['name'];
            $this->sku = $product['sku'];
            $this->description = $product['description'];
            $this->price = $product['price'];
            $this->specialPrice = $product['special_price'];
            $this->cost = $product['cost'];
            $this->qty = $product['qty'];
            return $this;
        }
    }

    public function loadAll(): array
    {
        $db = new SqlBuilder();
        $products = $db->select()->from('products')->getAll();
        return $products;
    }
    
    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function update(): void
    {
        $product = [
            'name' => $this->name,
            'description' => $this->description,
            'sku' => $this->sku,
            'price' => $this->price,
            'special_price' => $this->specialPrice,
            'cost' => $this->cost,
            'qty' => $this->qty,
        ];

        $productCategories = [];
        if(isset($this->categories)){
            foreach ($this->categories as $categoryId) {
                $productCategories[] = [
                    'product_id' => $this->id,
                    'category_id' => $categoryId
                ];
            }
        }

        $db = new SqlBuilder();
        $db->update('products')->set($product)->where('id', $this->id)->exec();

        self::findAndDeleteProductCategorieConn($this->id);
        self::addProductCategoriesRelations($productCategories);
    }

    private function create(): void
    {
        $product = [
            'name' => $this->name,
            'description' => $this->description,
            'sku' => $this->sku,
            'price' => $this->price,
            'special_price' => $this->specialPrice,
            'cost' => $this->cost,
            'qty' => $this->qty,
        ];

        $db = new SqlBuilder();
        $db->insert('products')->values($product)->exec();
    }

    public function delete()
    {   
        if (count($_POST) === 1 || count($_POST) === 2) {
            $id = Request::getArrayKey('first');
        }

        $db = new SqlBuilder();
        $allIds = $db->select('id')->from('products')->getAll();

        if($id !== null && in_array(['id'=>$id], $allIds) && Request::getArrayKey('last') === 'delete') {
            echo "You have just deleted product with id: " . $id . ".<br>";
            $db = new SqlBuilder();
            $db->delete()->from('products')->where('id', $id)->exec();
        }
    }

    public static function checkSkuUnique($sku)
    {
        $db = new SqlBuilder;
        $allSku = $db->select('sku')->from('products')->getAll();
        $allSku = array_merge_recursive(...array_values($allSku))["sku"];

        if(in_array($sku, $allSku) || $sku === "" || $sku === Null) {
            return false;
        } else {
            return true;
        }
    }

    public static function getAllCollumnsFromTable(string $collumn): array
    {
        $db = new SqlBuilder();
        $collumValues = $db->select($collumn)->from('products')->getAll();

        $collumValues = array_merge_recursive(...array_values($collumValues))[$collumn];

        return $collumValues;
    }

    public static function checkIfProductHasAssignedCategories(?int $id): ?int
    {
        $db = new SqlBuilder();
        $productId = $db->selectDistinct('product_id')->from('connections')->where('product_id', $id)->getAll();
        if ($productId !== []) {
            return $productId[0]['product_id'];
        } else return null;
    }

    public static function findAndDeleteProductCategorieConn(?int $id): void
    {
        $dbProductId = self::checkIfProductHasAssignedCategories($id);
        if ($id === $dbProductId && $dbProductId !== null) {
            $db = new SqlBuilder();
            $db->delete()->from('connections')->where('product_id', $id)->exec();
        }
    }

    public static function addProductCategoriesRelations(array $categories): void
    {
        foreach ($categories as $prodIdAndCatId) {
            $db = new SqlBuilder();
            $db->insert('connections')->values($prodIdAndCatId)->exec();
        }
    }
}