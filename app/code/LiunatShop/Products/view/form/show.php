<?php

use LiunatShop\Framework\Helpers\Request;
use LiunatShop\Products\Model\Collection\Products;

?>

<div class="wrapper">
    <h1><?= $data['title'] ?></h1>
    <?php echo $data['form']; ?>
</div>

<div>
    <?php
    $request = new Request();
    if (isset($data['products']) && $request->getPost('id') !== "") {
        $id = $data['products']->getId();
        $product = [
            'id' => $id,
            'name' => $data['products']->getName(),
            'description' => $data['products']->getDescription(),
            'sku' => $data['products']->getSku(),
            'price' => $data['products']->getPrice(),
            'specialPrice' => $data['products']->getSpecialPrice(),
            'cost' => $data['products']->getCost(),
            'qty' => $data['products']->getQty()
        ];
    }

    if ($data['products']->getId() !== null) {
        $table = "<table class='table-stripped'>";
        $table .= "<tr>";
        foreach ($product as $header => $value) :
            $table .= "<th>$header</th>";
        endforeach;
        $table .= "<th>Edit</th><th>Delete</th></tr><tr>";
        foreach ($product as $productProp) :
            $table .= "<td>$productProp</td>";
        endforeach;
        $table .= "<td><form action='http://www.shop.test/index.php/products/edit/$id' method='POST'>
                <button type='submit' name='$id'>Edit</button></form></td>";
        $table .= "<td><form action='http://www.shop.test/index.php/products/delete/$id' method='POST'>
                <button type='submit' name='$id'>Delete</button></form></td></tr>";
        $table .= "</tr></table>";
        echo $table;
    }
    ?>
</div>

<div>
    <?php
    $products = new Products();
    $allProducts = $products->getAllProducts();

    $table = "<table class='table-stripped'>";
    $table .= "<tr>";
    foreach ($allProducts[0] as $header => $value) {
        $table .= "<th>$header</th>";
    }
    $table .= "<th>Edit</th><th>Delete</th></tr>";

    foreach ($allProducts as $product) {
        $table .= "<tr>";
        foreach ($product as $productPropertyValue) {
            $id = $product['id'];
            $table .= "<td>$productPropertyValue</td>";
        }
        $table .= "<td><form action='http://www.shop.test/index.php/products/edit/$id' method='POST'>
            <button type='submit' name='$id'>Edit</button></form></td>";
        $table .= "<td><form action='http://www.shop.test/index.php/products/delete/$id' method='POST'>
            <button type='submit' name='$id'>Delete</button></form></td></tr>";
    }
    $table .= "</tr></table>";
    echo $table;
    ?>
</div>