<?php

$routes = [
    'products' => 'LiunatShop\Products\Controller\Index',
    'categories' => 'LiunatShop\Categories\Controller\Index',
    'home' => 'LiunatShop\Home\Controller\Index'
];