<?php

require_once '../app/config/env.php';
require_once '../vendor/autoload.php';

$request = new \LiunatShop\Framework\Helpers\Request;
$routes = new \LiunatShop\Framework\Helpers\Routing;

$url = $request->getRoute();


$routes->getControllerClass($url);